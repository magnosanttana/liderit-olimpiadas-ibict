<?php 
    $idCategoria = $_GET['id'] ?? 0;
    $result = getCategorias($idCategoria);
    $categorias = $result['categorias'];
    $tipo = '';
    if(!empty($_GET['type']) && $_GET['type'] == 'INDICATOR'){
        $tipo = "Indicadores";
    }

    if(!empty($_GET['type']) && $_GET['type'] == 'LAYER'){
        $tipo = "Camadas";
    }
?>
<div class="row">
    <div class="col-xxl-4 col-md-12">
        <div class="card info-card sales-card">
        <div class="card-body">
            <?php //var_dump(http_build_query($_GET)); ?>
            <h5 class="card-title"><?php echo $tipo; ?> da categoria "<?php echo $_GET['n']; ?>"</h5>
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($categorias as $indicador): ?>
                    <tr>
                        <th scope="row"><?php echo $indicador->id; ?></th>
                        <td><?php echo $indicador->name; ?></td>
                        <td>
                            <!-- <button type="button" class="btn btn-info"><i class="bi bi-pencil-square"></i></button> -->
                            <a href="?<?php echo http_build_query($_GET); ?>&idGroupIndicador=<?php echo $indicador->id; ?>" class="btn btn-info"><i class="bi bi-pencil-square"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        </div>
    </div>

    <div class="col-xxl-8 col-md-12">
        <div class="card info-card revenue-card">

        <div class="card-body">
            <h4 class="card-title">Detalhes</h4>
            <?php 
                if(!empty($_GET['idGroupIndicador']) && $_GET['type'] == 'INDICATOR'){
                    include_once("form-indicador.php");
                }

                if(!empty($_GET['idGroupIndicador']) && $_GET['type'] == 'LAYER'){
                    include_once("form-camada.php");
                }
            ?>
            
        </div>

        </div>
    </div>
</div> 
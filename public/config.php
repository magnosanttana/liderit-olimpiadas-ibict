<?php

require("vendor/autoload.php");

use GuzzleHttp\Client;

$hashAuthorization = 'SWdvclJvZHJpZ3VlczpqRGY0MjlvaW5Bckcj';

const URL_GET_VISAO = 'https://beta.visao.ibict.br/api2/grup-categories';
const URL_GET_CATEGORIAS = 'https://beta.visao.ibict.br/api2/categories';
const URL_GET_INDICADOR = 'https://beta.visao.ibict.br/api2/grup-indicators';
const URL_GET_REGIOES = 'https://beta.visao.ibict.br/api2/regions';
const URL_ACCESS_TOKEN = 'https://beta.visao.ibict.br/api2/session/getAccessToken';
const URL_UPDATE_INDICADOR = 'https://beta.visao.ibict.br/api2/grup-indicators';
const URL_GET_CAMADA = 'https://beta.visao.ibict.br/api2/group-layers';
const URL_UPDATE_CAMADA = 'https://beta.visao.ibict.br/api2/group-layers';

echo "<pre>";

function getVisao($idVisao)
{
    $client = new Client();

    try {
        $response = $client->get(URL_GET_VISAO . '/'. $idVisao);
        $stringBody = $response->getBody();
        $visao = json_decode($stringBody->getContents());

        $categories = $visao->categories;
        $indicadores = [];
        $camadas = [];

        foreach ($categories as $categoria) {
            if(isset($categoria->cod->category->type) && $categoria->cod->category->type == 'INDICATOR'){
                $indicadores[] = $categoria->cod->category;
            }

            if(isset($categoria->cod->category->type) && $categoria->cod->category->type == 'LAYER'){
                $camadas[] = $categoria->cod->category;
            }
        }

        return [
            'status' => true,
            'id' => $visao->id,
            'nome' => $visao->about,
            'tipoMapa' => $visao->tipoMapa, 
            'ownerId' => $visao->ownerId,
            'indicadores' => $indicadores,
            'camadas' => $camadas
        ];
    } catch (GuzzleHttp\Exception\ClientException $e) {
        return ['status' => false, 'msg' => $e->getMessage(), 'id' => null];
        //throw $th;
    }
    

}

function getCategorias($idCategoria)
{
    $client = new Client();

    try {
        $response = $client->get(URL_GET_CATEGORIAS . '/'. $idCategoria . '/content');
        $stringBody = $response->getBody();
        $categorias = json_decode($stringBody->getContents());

        return [
            'status' => true,
            'categorias' => $categorias
        ];
    } catch (GuzzleHttp\Exception\ClientException $e) {
        return ['status' => false, 'msg' => $e->getMessage(), 'categorias' => []];
    }

}

function getIndicador($idIndicador)
{
    $client = new Client();

    try {
        $response = $client->get(URL_GET_INDICADOR . '/'. $idIndicador);
        $stringBody = $response->getBody();
        $indicador = json_decode($stringBody->getContents());

        return [
            'status' => true,
            'indicador' => $indicador
        ];
    } catch (GuzzleHttp\Exception\ClientException $e) {
        return ['status' => false, 'msg' => $e->getMessage(), 'categorias' => []];
    }

}

function getRegioes($tipoRegiao)
{
    $client = new Client();

    try {
        $response = $client->get(URL_GET_REGIOES . '/'. $tipoRegiao);
        $stringBody = $response->getBody();
        $regioes = json_decode($stringBody->getContents());

        return [
            'status' => true,
            'regioes' => $regioes
        ];
    } catch (GuzzleHttp\Exception\ClientException $e) {
        return ['status' => false, 'msg' => $e->getMessage(), 'categorias' => []];
    }

}

function updateIndicador($dados)
{
    $client = new Client(['cookies' => true]);

    #print_r($dados);
    #print_r($indicadorData);
    #print_r(array_diff($indicadorData, $dados));

    try {

        $client->post(URL_ACCESS_TOKEN, ['headers' => ['Authorization' => 'Basic SWdvclJvZHJpZ3VlczpqRGY0MjlvaW5Bckcj']]);

        $request_options = [
            'headers' => ['Content-type' => 'application/json'],
            'json' => $dados,
          ];
        
        $response = $client->request('PUT', URL_UPDATE_INDICADOR, $request_options);

        $stringBody = $response->getBody();
        $indicador = json_decode($stringBody->getContents());

        return [
            'status' => true,
            'indicador' => $indicador
        ];
    } catch (Exception $e) {
        return ['status' => false, 'msg' => 'Erro: Os dados enviados podem estar inválidos. Verifique se os campos estão preenchidos corretamente e se não está adicionando regiões duplicadas. Log do erro: ' .$e->getMessage(), 'indicador' => null];
    }

}

function getCamada($idCamada)
{
    $client = new Client();

    try {
        $response = $client->get(URL_GET_CAMADA . '/'. $idCamada);
        $stringBody = $response->getBody();
        $camada = json_decode($stringBody->getContents());
        
        return [
            'status' => true,
            'camada' => $camada
        ];
    } catch (GuzzleHttp\Exception\ClientException $e) {
        return ['status' => false, 'msg' => $e->getMessage(), 'categorias' => []];
    }

}

function updateCamada($dados)
{
    $client = new Client(['cookies' => true]);

    #print_r($indicadorData);
    #print_r(array_diff($indicadorData, $dados));

    try {

        $client->post(URL_ACCESS_TOKEN, ['headers' => ['Authorization' => 'Basic SWdvclJvZHJpZ3VlczpqRGY0MjlvaW5Bckcj']]);

        $request_options = [
            'headers' => ['Content-type' => 'application/json'],
            'json' => $dados,
          ];
        
        $response = $client->request('PUT', URL_UPDATE_CAMADA, $request_options);

        $stringBody = $response->getBody();
        $camada = json_decode($stringBody->getContents());

        return [
            'status' => true,
            'camada' => $camada
        ];
    } catch (Exception $e) {
        return ['status' => false, 'msg' => 'Erro: Os dados enviados podem estar inválidos. Verifique se os campos estão preenchidos corretamente e se não está adicionando regiões duplicadas. Log do erro:' . $e->getMessage(), 'camada' => null];
    }

}


echo "</pre>";

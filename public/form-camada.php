<?php 
    $idCamada = $_GET['idGroupIndicador'] ?? 0;
    $result = getCamada($idCamada);
    $camada = $result['camada'];

    /* 
    <!-- 

{
                            "groupLayerId": groupLayer.id,
                            "groupLayerName": groupLayer.name,
                            "bigAreaId": groupLayer.bigArea.id,
                            "groupLayerKeyWord": groupLayer.keyword,
                            "groupLayerDescription": groupLayer.description,
                            "groupLayerActive": true,
                            "groupLayerTypeOfLayer": "MARKER",
                            "groupLayerPermission": "PRIVATE",
                            "layerList": groupLayer.layersJson
}


    -->
    */

    if(!empty($_POST['a']) && $_POST['a'] == 'updateCamada'){
        $dadosUpdate = [
            'groupLayerId' => (int) $_POST['groupLayerId'],
            'groupLayerName' => $_POST['groupLayerName'],
            'bigAreaId' => (int) $_POST['bigAreaId'],
            'groupLayerKeyWord' => $_POST['groupLayerKeyWord'],
            'groupLayerDescription' => $_POST['groupLayerDescription'],
            'groupLayerActive' => (bool) $_POST['groupLayerActive'],
            'groupLayerTypeOfLayer' => $_POST['groupLayerTypeOfLayer'],
            'groupLayerPermission' => $_POST['groupLayerPermission'],
            ];
       

        $totalNomes = count($_POST['nome']);

        for($r = 0; $r < $totalNomes; $r++){
            $rowCamada = [
                'name' => $_POST['nome'][$r],
                'geoJson' => "[".$_POST['latitude'][$r].",".$_POST['longitude'][$r]."]",
                'description' => $_POST['descricao'][$r],
                'source' => $_POST['fonte'][$r],
            ];

            $dadosUpdate['layerList'][] = $rowCamada;
        }

        $resultUpdate = updateCamada($dadosUpdate);

        if($resultUpdate['status']){
            $result = getCamada($idCamada);
            $camada = $result['camada'];

        }
        echo "</pre>";

        
    }


?>
<form method="post">
    <?php if(isset($resultUpdate) && $resultUpdate['status']): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            Atualizado com sucesso!
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>
    <?php if(isset($resultUpdate['msg']) && $resultUpdate['msg']): ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <?php echo $resultUpdate['msg']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>
    <input type="hidden" name="a" value="updateCamada">
    <!-- 

{
                            "groupLayerId": groupLayer.id,
                            "groupLayerName": groupLayer.name,
                            "bigAreaId": groupLayer.bigArea.id,
                            "groupLayerKeyWord": groupLayer.keyword,
                            "groupLayerDescription": groupLayer.description,
                            "groupLayerActive": true,
                            "groupLayerTypeOfLayer": "MARKER",
                            "groupLayerPermission": "PRIVATE",
                            "layerList": groupLayer.layersJson
}


    -->
    <input type="hidden" name="groupLayerId" value="<?php echo $camada->id;?>">
    <input type="hidden" name="groupLayerName" value="<?php echo $camada->name;?>">
    <input type="hidden" name="bigAreaId" value="<?php echo $camada->bigArea->id;?>">
    <input type="hidden" name="bigAreaName" value="<?php echo $camada->bigArea->name;?>">
    <input type="hidden" name="groupLayerKeyWord" value="<?php echo $camada->keyWord;?>">
    <input type="hidden" name="groupLayerDescription" value="<?php echo $camada->description;?>">
    <input type="hidden" name="groupLayerActive" value="<?php echo $camada->active;?>">
    <input type="hidden" name="groupLayerTypeOfLayer" value="<?php echo $camada->layersJson[0]->type;?>">
    <input type="hidden" name="groupLayerPermission" value="<?php echo $camada->permission;?>">

    <div class="row mb-3">
        <label for="inputText" class="col-sm-2 col-form-label">Nome</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" disabled value="<?php echo $camada->name;?>">
        </div>
    </div>
    <div class="row mb-3">
        <label for="inputEmail" class="col-sm-2 col-form-label">Descrição</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" disabled value="<?php echo $camada->description;?>">
        </div>
    </div>
    <div class="row mb-3">
        <label for="inputEmail" class="col-sm-2 col-form-label">Tipo</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" disabled value="<?php echo $camada->layersJson[0]->type;?>">
        </div>
    </div>
    <h5 class="card-title">Camadas</h5>
    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nome</th>
            <th scope="col">Latitude</th>
            <th scope="col">Longitude</th>
            <th scope="col">Descrição</th>
            <th scope="col">Fonte</th>
            <th scope="col">
                <button id="addCamada" type="button" class="btn btn-success"><i class="bi bi-plus-square me-1"></i> Adicionar</button>
            </th>
            </tr>
        </thead>
        <tbody id="tbodyCamadas">
            <?php 
                $i = 1;
                foreach($camada->layersJson[0]->dataset as $item): 
            ?>
                <tr id="trCamada<?php echo $i;?>" class="trCamadas">
                    <th scope="row" id="ref<?php echo $i;?>"><?php echo $i;?></th>
                    <td>
                        <input type="text" class="form-control" id="nome<?php echo $i;?>" name="nome[]" value="<?php echo $item->name;?>">
                    </td>
                    <td>
                    <?php 
                        $coordenadas = json_decode($item->geoJson);
                    ?>
                        <input type="text" class="form-control" id="latitude<?php echo $i;?>" name="latitude[]" value="<?php echo $coordenadas[0];?>">
                    </td>
                    <td>
                        <input type="text" class="form-control" id="longitude<?php echo $i;?>" name="longitude[]" value="<?php echo $coordenadas[1];?>">
                    </td>
                    <td>
                        <input type="text" class="form-control" id="descricao<?php echo $i;?>" name="descricao[]" value="<?php echo $item->description;?>">
                    </td>
                    <td>
                        <input type="text" class="form-control" id="fonte<?php echo $i;?>" name="fonte[]" value="<?php echo $item->source;?>">
                    </td>
                    <td>
                        <button type="button" id="btnExcluir<?php echo $i;?>" data-linha="<?php echo $i;?>" class="btn btn-danger btn-delete-linha"><i class="bi bi-dash-square"></i></button>
                    </td>
                </tr>
            <?php $i++; endforeach; ?>
        </tbody>
    </table>
    
   
    <div class="row mb-3">
        <div class="col-sm-10">
        <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </div>

</form>
<script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
<script>
    $(document).ready(function() {
        console.log('carregou');
         let num = $(".trCamadas").length;
        /* $('#addCamada').click(function() {
            var newNum  = new Number(num + 1);

            var newSection = $("#trCamada" + num).clone().attr("id", "trCamada" + newNum);
            $("#tbodyCamadas").append(newSection)
            $("#trCamada"+newNum+ " th").html(newNum);

            $("#trCamada"+newNum+ " #latitude"+num).attr("id", "latitude" + newNum);
            $("#trCamada"+newNum+ " #longitude"+num).attr("id", "longitude" + newNum);
            $("#trCamada"+newNum+ " #nome"+num).attr("id", "nome" + newNum);
            $("#trCamada"+newNum+ " #descricao"+num).attr("id", "descricao" + newNum);
            $("#trCamada"+newNum+ " #fonte"+num).attr("id", "fonte" + newNum);
            $("#trCamada"+newNum+ " #btnExcluir"+num).attr("data-linha", newNum);
            $("#trCamada"+newNum+ " #btnExcluir"+num).attr("id", "btnExcluir" + newNum);

            $("#latitude"+newNum).val('');
            $("#longitude"+newNum).val('');
            $("#nome"+newNum).val('');
            $("#descricao"+newNum).val('');
            $("#fonte"+newNum).val('');

            num = new Number(num + 1);

        }); */
 
        /* $('.btn-delete-linha').on('click', function() {
            
            var num = $(this).data('linha');

            $('#input' + num).remove();
            $('#add').attr('disabled','');

            if (num-1 == 1)
                $('#remove').attr('disabled','disabled');
        }); */

       // $('#remove').attr('disabled','disabled');
    });
    $(document).on('click', '.btn-delete-linha', function() {
        console.log('clicou em excluir linha', $(this).data('linha'));
        var linha = $(this).data('linha');

        $("#trCamada"+linha).remove();
    });

    $(document).on('click', '#addCamada', function() {
        let num = $(".trCamadas").length;
        console.log('clicou em add linha', num);
        var newNum  = new Number(num + 1);

        var newSection = $("#trCamada" + num).clone().attr("id", "trCamada" + newNum);
        $("#tbodyCamadas").append(newSection)
        $("#trCamada"+newNum+ " th").html(newNum);

        $("#trCamada"+newNum+ " #latitude"+num).attr("id", "latitude" + newNum);
        $("#trCamada"+newNum+ " #longitude"+num).attr("id", "longitude" + newNum);
        $("#trCamada"+newNum+ " #nome"+num).attr("id", "nome" + newNum);
        $("#trCamada"+newNum+ " #descricao"+num).attr("id", "descricao" + newNum);
        $("#trCamada"+newNum+ " #fonte"+num).attr("id", "fonte" + newNum);
        $("#trCamada"+newNum+ " #btnExcluir"+num).attr("data-linha", newNum);
        $("#trCamada"+newNum+ " #btnExcluir"+num).attr("id", "btnExcluir" + newNum);

        $("#latitude"+newNum).val('');
        $("#longitude"+newNum).val('');
        $("#nome"+newNum).val('');
        $("#descricao"+newNum).val('');
        $("#fonte"+newNum).val('');

        num = new Number(num + 1);
    });
</script>
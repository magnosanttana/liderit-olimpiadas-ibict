<?php 
    $idIndicador = $_GET['idGroupIndicador'] ?? 0;
    $result = getIndicador($idIndicador);
    $indicador = $result['indicador'];

    $resultRegioes = getRegioes($indicador->typeRegion);
    $regioes = $resultRegioes['regioes'];

    if(!empty($_POST['a']) && $_POST['a'] == 'updateIndicador'){
        $dadosUpdate = [
            'id' => (int) $_POST['id'],
            'name' => $_POST['name'],
            'typeId' => (int) $_POST['typeId'],
            'bigAreaId' => (int) $_POST['bigAreaId'],
            'keyWords' => $_POST['keyWords'],
            'description' => $_POST['description'],
            'source' => '',
            'active' => (bool) $_POST['active'],
            'typeRegion' => $_POST['typeRegion'],
            ];
       

        $totalRegioes = count($_POST['regiao']);

        for($r = 0; $r < $totalRegioes; $r++){
            $rowIndicador = [
                'region' => (int) $_POST['regiao'][$r],
                'value' => (int) $_POST['valor'][$r],
                'date' => $_POST['periodo'][$r].'T00:00:00Z',
            ];

            $dadosUpdate['indicatorList'][] = $rowIndicador;
        }
        echo "<pre>";
        $resultUpdate = updateIndicador($dadosUpdate);

        if($resultUpdate['status']){
            $result = getIndicador($idIndicador);
            $indicador = $result['indicador'];

            $resultRegioes = getRegioes($indicador->typeRegion);
            $regioes = $resultRegioes['regioes'];
        }
        //print_r($resultUpdate);
        echo "</pre>";

        
    }

    $arrTiposDados = [
        1 => 'Dólar',
        2 => '1',
        3 => 'Nível MC',
        4 => 'Porcentagem',
        5 => 'Ativo',
        6 => 'R$',
        7 => 'Bilhões de dólares',
        8 => 'Tipo de lixo',
        9 => 'Ativo alternativo',
        10 => 'Ocasionalidade',
        12 => 'Classe',
        13 => '142PMGIRSResponsavel',
        14 => 'T por ano',
    ];
?>
<form method="post">
    <?php if(isset($resultUpdate) && $resultUpdate['status']): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            Atualizado com sucesso!
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>
    <?php if(isset($resultUpdate['msg']) && $resultUpdate['msg']): ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <?php echo $resultUpdate['msg']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php endif; ?>
    <input type="hidden" name="a" value="updateIndicador">
    <!-- 

'id' => 2458,
    'name' => 'Olimpiadas Cientificas no Brasil',
    'typeId' => 2,
    'bigAreaId' => 8,
    'keyWords' => 'educacao, olimpiadas cientificas, ciencias, conhecimento edit',
    'description' => 'Quantidade de Olimpiadas Cientificas no Brasil edit',
    'source' => '',
    'active' => true,
    'typeRegion' => 'ESTADO',

    -->
    <input type="hidden" name="id" value="<?php echo $indicador->id;?>">
    <input type="hidden" name="name" value="<?php echo $indicador->name;?>">
    <input type="hidden" name="typeId" value="<?php echo $indicador->typeId;?>">
    <input type="hidden" name="bigAreaId" value="<?php echo $indicador->bigAreaId;?>">
    <input type="hidden" name="keyWords" value="<?php echo $indicador->keyWords;?>">
    <input type="hidden" name="description" value="<?php echo $indicador->description;?>">
    <input type="hidden" name="source" value="<?php echo $indicador->source;?>">
    <input type="hidden" name="active" value="<?php echo $indicador->active;?>">
    <input type="hidden" name="typeRegion" value="<?php echo $indicador->typeRegion;?>">

    <div class="row mb-3">
        <label for="inputText" class="col-sm-2 col-form-label">Nome</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" disabled value="<?php echo $indicador->name;?>">
        </div>
    </div>
    <div class="row mb-3">
        <label for="inputEmail" class="col-sm-2 col-form-label">Descrição</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" disabled value="<?php echo $indicador->description;?>">
        </div>
    </div>
    <div class="row mb-3">
        <label for="inputEmail" class="col-sm-2 col-form-label">Tipo Região</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" disabled value="<?php echo $indicador->typeRegion;?>">
        </div>
    </div>
    <div class="row mb-3">
        <label for="inputEmail" class="col-sm-2 col-form-label">Tipo de dado</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" disabled value="<?php echo $arrTiposDados[$indicador->typeId];?>">
        </div>
    </div>
    <h5 class="card-title">Indicadores</h5>
    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Região</th>
            <th scope="col">Valor</th>
            <th scope="col">Periodo</th>
            <th scope="col">
                <button id="addIndicador" type="button" class="btn btn-success"><i class="bi bi-plus-square me-1"></i> Adicionar</button>
            </th>
            </tr>
        </thead>
        <tbody id="tbodyIndicadores">
            <?php 
                $i = 1;
                foreach($indicador->indicatorList as $item): 
                    //print_r($item);
            ?>
                <tr id="trIndicador<?php echo $i;?>" class="trIndicadores">
                    <th scope="row" id="ref<?php echo $i;?>"><?php echo $i;?></th>
                    <td>
                        <select class="form-select" aria-label="Default select example" id="regiao<?php echo $i;?>" name="regiao[]">
                            <?php foreach($regioes as $regiao): ?>
                            <option 
                            <?php if($regiao->geoCode == $item->region) echo "selected"; ?>
                            value="<?php echo $regiao->geoCode;?>"><?php echo $regiao->name;?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" class="form-control" id="valor<?php echo $i;?>" name="valor[]" value="<?php echo $item->value;?>">
                    </td>
                    <td>
                    <?php 
                        $date = new DateTime($item->date);
                    ?>
                        <input type="date" class="form-control" id="periodo<?php echo $i;?>" name="periodo[]" value="<?php echo $date->format('Y-m-d');?>">
                    </td>
                    <td>
                        <button type="button" id="btnExcluir<?php echo $i;?>" data-linha="<?php echo $i;?>" class="btn btn-danger btn-delete-linha"><i class="bi bi-dash-square"></i></button>
                    </td>
                </tr>
            <?php $i++; endforeach; ?>
        </tbody>
    </table>
    
   
    <div class="row mb-3">
        <div class="col-sm-10">
        <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </div>

</form>
<script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
<script>
    $(document).ready(function() {
        console.log('carregou');
        let num = $(".trIndicadores").length;
        $('#addIndicador').click(function() {
            //var num     = $(".trIndicadores").length;
            var newNum  = new Number(num + 1);

            var newSection = $("#trIndicador" + num).clone().attr("id", "trIndicador" + newNum);
            $("#tbodyIndicadores").append(newSection)
            $("#trIndicador"+newNum+ " th").html(newNum);

            $("#trIndicador"+newNum+ " #regiao"+num).attr("id", "regiao" + newNum);
            $("#trIndicador"+newNum+ " #valor"+num).attr("id", "valor" + newNum);
            $("#trIndicador"+newNum+ " #periodo"+num).attr("id", "periodo" + newNum);
            $("#trIndicador"+newNum+ " #btnExcluir"+num).attr("data-linha", newNum);
            $("#trIndicador"+newNum+ " #btnExcluir"+num).attr("id", "btnExcluir" + newNum);

            $("#valor"+newNum).val('');
            $("#regiao"+newNum).val('');
            $("#periodo"+newNum).val('');

            num = new Number(num + 1);

        });

        /* $('.btn-delete-linha').on('click', function() {
            
            var num = $(this).data('linha');

            $('#input' + num).remove();
            $('#add').attr('disabled','');

            if (num-1 == 1)
                $('#remove').attr('disabled','disabled');
        }); */

        $('#remove').attr('disabled','disabled');
    });
    $(document).on('click', '.btn-delete-linha', function() {
        console.log('clicou em excluir linha', $(this).data('linha'));
        var linha = $(this).data('linha');

        $("#trIndicador"+linha).remove();
    });
</script>
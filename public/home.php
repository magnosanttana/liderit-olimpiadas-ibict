<?php 
    if(!$idVisao):
?>  
    <div class="row">
    
    <div class="col-lg-12">
        <div class="card">
        <div class="card-body">
            <h5 class="card-title">Nenhuma Visão Selecionada</h5>
            <p>Informe acima qual o ID da visão que deseja editar os indicadores</p>
            <?php 
            if(isset($visao['msg']) && $visao['msg']):
            ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <?php echo $visao['msg']; ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <?php 
            endif;
            ?>
        </div>
        </div>
    </div>
    </div>
    <?php 
    endif;
    if($idVisao):
        include_once("visao-resumo.php");
    endif;
    ?> 
<?php
// visao-edita-indicador.php

require_once('visao-config.php');

function fetchIndicatorDetails($url, $authorizationHeader) {
    $options = [
        'http' => [
            'header' => "Authorization: Basic $authorizationHeader",
        ],
    ];

    $context = stream_context_create($options);
    $response = file_get_contents($url, false, $context);

    if ($response !== false) {
        return json_decode($response, true);
    } else {
        return false;
    }
}

function updateIndicatorData($url, $authorizationHeader, $data) {
    $options = [
        'http' => [
            'method' => 'PUT',
            'header' => [
                "Content-type: application/json",
                "Authorization: Basic $authorizationHeader",
            ],
            'content' => json_encode($data),
        ],
    ];

    $context = stream_context_create($options);
    $response = file_get_contents($url, false, $context);

    if ($response !== false) {
        return json_decode($response, true);
    } else {
        return false;
    }
}

// URL para obter os detalhes do indicador
$indicatorId = isset($_GET['id']) ? $_GET['id'] : null;
$urlGetIndicator = "https://beta.visao.ibict.br/api2/grup-indicators/$indicatorId";

// Obtém os detalhes do indicador
$indicatorDetails = fetchIndicatorDetails($urlGetIndicator, $hashAuthorization);

// Verifica se o formulário foi enviado para atualizar os dados
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Recebe os dados do formulário
    $updatedData = [
        'id' => $indicatorId,
        // Adicione os campos que podem ser atualizados conforme necessário
    ];

    // Adiciona ou atualiza os itens na lista de indicadores
    $newIndicators = [];
    $regions = $_POST['regions'] ?? [];
    $values = $_POST['values'] ?? [];

    foreach ($regions as $index => $region) {
        if (!empty($region) && isset($values[$index])) {
            $newIndicators[] = [
                'region' => $region,
                'value' => $values[$index],
                'date' => date('Y-m-d\TH:i:s\Z'),
            ];
        }
    }

    $updatedData['indicatorList'] = array_merge($indicatorDetails['indicatorList'], $newIndicators);

    echo "<pre>";
    print_r($updatedData);

    // Atualiza os dados do indicador
    $urlUpdateIndicator = 'https://beta.visao.ibict.br/api2/grup-indicators';
    $updatedIndicator = updateIndicatorData($urlUpdateIndicator, $hashAuthorization, $updatedData);

    print_r($updatedIndicator);
    exit();


    // Verifica se a atualização foi bem-sucedida
    if ($updatedIndicator !== false) {
        // Redireciona para a página de detalhes do indicador
        header("Location: visao-edita-indicador.php?id=$indicatorId");
        exit;
    } else {
        echo '<p style="color: red;">Erro ao atualizar os dados do indicador.</p>';
    }
}
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Indicador</title>
    <link rel="stylesheet" href="visao-style.css">
</head>
<body>
    <h1>Editar Indicador</h1>

    <?php
    if ($indicatorDetails !== false) {
    ?>
    <form method="post">
        <label for="name">Nome:</label>
        <input type="text" id="name" name="name" value="<?= $indicatorDetails['name'] ?>" readonly>

        <label for="description">Descrição:</label>
        <textarea id="description" name="description" readonly><?= $indicatorDetails['description'] ?></textarea>

        <label for="keyWords">Palavras-chave:</label>
        <input type="text" id="keyWords" name="keyWords" value="<?= $indicatorDetails['keyWords'] ?>" readonly>

        <label for="source">Fonte:</label>
        <input type="text" id="source" name="source" value="<?= $indicatorDetails['source'] ?>" readonly>

        <label for="typeRegion">Região do Tipo:</label>
        <input type="text" id="typeRegion" name="typeRegion" value="<?= $indicatorDetails['typeRegion'] ?>" readonly>

        <label for="ownerName">Nome do Proprietário:</label>
        <input type="text" id="ownerName" name="ownerName" value="<?= $indicatorDetails['ownerName'] ?>" readonly>

        <!-- Adicione outros campos do indicador conforme necessário -->

        <h2>Indicadores</h2>

        <?php foreach ($indicatorDetails['indicatorList'] as $index => $indicator) { ?>
    <div>
        <label for="region<?= $index ?>">Região:</label>
        <input type="text" id="region<?= $index ?>" name="regions[]" value="<?= $indicator['region'] ?>" readonly>

        <label for="value<?= $index ?>">Valor:</label>
        <input type="text" id="value<?= $index ?>" name="values[]" value="<?= $indicator['value'] ?>" readonly>
    </div>
<?php } ?>

<!-- Adicione campos adicionais para novos indicadores -->

<div>
    <label for="newRegion">Nova Região:</label>
    <select id="newRegion" name="newRegion">
        <!-- Adicione opções do select com os nomes correspondentes aos códigos numéricos -->
        <option value="codigo1">Nome1</option>
        <option value="codigo2">Nome2</option>
        <!-- Adicione mais opções conforme necessário -->
    </select>

    <label for="newValue">Novo Valor:</label>
    <input type="text" id="newValue" name="newValue">
    <button type="button" onclick="addNewIndicator()">Adicionar</button>
</div>

<script>
function addNewIndicator() {
    var newRegion = document.getElementById('newRegion').value;
    var newValue = document.getElementById('newValue').value;

    if (newRegion && newValue) {
        var newIndicatorDiv = document.createElement('div');

        var newRegionInput = document.createElement('input');
        newRegionInput.type = 'text';
        newRegionInput.name = 'regions[]';
        newRegionInput.value = newRegion;
        newRegionInput.readOnly = true;

        var newValueInput = document.createElement('input');
        newValueInput.type = 'text';
        newValueInput.name = 'values[]';
        newValueInput.value = newValue;
        newValueInput.readOnly = true;

        newIndicatorDiv.appendChild(newRegionInput);
        newIndicatorDiv.appendChild(newValueInput);

        document.querySelector('form').appendChild(newIndicatorDiv);

        // Limpar os campos após adicionar o novo indicador
        document.getElementById('newRegion').value = '';
        document.getElementById('newValue').value = '';
    } else {
        alert('Por favor, preencha a região e o valor antes de adicionar.');
    }
}
</script>

<button type="submit">Salvar</button>
</form>
    <?php
    } else {
        echo '<p style="color: red;">Erro ao obter os detalhes do indicador.</p>';
    }
    ?>
</body>
</html>
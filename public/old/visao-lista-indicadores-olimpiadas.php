<?php
// index.php

require_once('visao-config.php');

// Função para obter os dados da API
function fetchDataFromApi($url, $authorizationHeader) {
    $options = [
        'http' => [
            'header' => "Authorization: Basic $authorizationHeader",
        ],
    ];

    $context = stream_context_create($options);
    $response = file_get_contents($url, false, $context);

    if ($response !== false) {
        return json_decode($response, true);
    } else {
        return false;
    }
}

// URL para os indicadores e camadas
$urlIndicators = 'https://beta.visao.ibict.br/api2/categories/' . $idIndicators . '/content';
$urlLayers = 'https://beta.visao.ibict.br/api2/categories/' . $idLayers . '/content';

// Obtém a lista de indicadores
$indicators = fetchDataFromApi($urlIndicators, $hashAuthorization);

// Obtém a lista de camadas
$layers = fetchDataFromApi($urlLayers, $hashAuthorization);
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Indicadores e Camadas</title>
    <link rel="stylesheet" href="visao-style.css">
</head>
<body>
    <h1>Lista de Indicadores e Camadas</h1>

    <h2>Indicadores:</h2>
    <ul>
        <?php
        if ($indicators !== false) {
            $layerType = 'Indicador';
            foreach ($indicators as $indicator) {
                echo '<li><a href=/visao-edita-indicador.php?id=' . $indicator['id'] . '&type=' . $layerType . ' alt="Indicador ID: ' . $indicator['id'] . '">' . $indicator['name'] . '</a></li>';
            }
        } else {
            echo '<p style="color: red;">Erro ao obter a lista de indicadores.</p>';
        }
        ?>
    </ul>

    <h2>Camadas:</h2>
    <ul>
        <?php
        if ($layers !== false) {
            $layerType = 'Camada';            
            foreach ($layers as $layer) {
                echo '<li><a href=/visao-edita-camada.php?id=' . $layer['id'] . '&type=' . $layerType . ' alt="Camada ID: ' . $layer['id'] . '">' . $layer['name'] . '</a></li>';
            }
        } else {
            echo '<p style="color: red;">Erro ao obter a lista de camadas.</p>';
        }
        ?>
    </ul>
</body>
</html>

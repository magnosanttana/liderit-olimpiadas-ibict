<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Indicadores do Sistema</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            background-color: #f4f4f4;
        }

        h1 {
            text-align: center;
        }

        ul {
            list-style-type: none;
            padding: 0;
        }

        li {
            margin-bottom: 10px;
        }

        a {
            display: block;
            padding: 10px;
            background-color: #fff;
            text-decoration: none;
            color: #333;
            border: 1px solid #ddd;
            border-radius: 5px;
        }

        a:hover {
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>
    <h1>Lista de Indicadores do Sistema</h1>

    <?php
    // Verifica se foi passado um ID na URL
    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        // Substitua 'URL_DA_API_EXTERNA', 'SEU_USUARIO' e 'SUA_SENHA' pelos valores reais
        $url_api = 'https://beta.visao.ibict.br/api2/group-layers/' . urlencode($id);
        $usuario = 'SEU_USUARIO';
        $senha = 'SUA_SENHA';
        
        print_r($url_api . "<br>");

        // Configuração do cabeçalho para Basic Authentication
        $options = [
            'http' => [
                'header' => "Authorization: Basic SWdvclJvZHJpZ3VlczpqRGY0MjlvaW5Bckcj",
            ],
        ];

        $context = stream_context_create($options);

        // Faz a solicitação GET à API externa usando o contexto com a autenticação
        $response = file_get_contents($url_api, false, $context);
        echo '<p>Código de Status da API: ' . http_response_code() . '</p>';

        if ($response !== false) {
            $data = json_decode($response, true);

            // Verifica se a resposta é um JSON válido
            if ($data !== null && isset($data['name'])) {
                // Monta a lista de links com os indicadores
                echo '<ul>';
                // Exibe um link usando 'name', 'description' e 'ownerId' como parâmetro
                $link = htmlspecialchars($data['name']);
                $alt = isset($data['description']) ? htmlspecialchars($data['description']) : '';
                $ownerId = isset($data['ownerId']) ? htmlspecialchars($data['ownerId']) : '';
                echo '<li><a href="' . $link . '" alt="' . $alt . '?ownerId=' . $ownerId . '">' . $data['name'] . '</a></li>';
                echo '</ul>';
            } else {
                echo '<p style="color: red;">Erro ao decodificar a resposta da API ou campo "name" ausente.</p>';
            }
        } else {
            echo '<p style="color: red;">Erro na solicitação à API: ' . error_get_last()['message'] . '</p>';
        }
    } else {
        echo '<p style="color: red;">ID não especificado na URL.</p>';
    }
    ?>
</body>
</html>

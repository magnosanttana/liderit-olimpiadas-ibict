<div class="row">
    <div class="col-xxl-6 col-md-12">
        <div class="card info-card sales-card">
        <div class="card-body">
            <h5 class="card-title">Categorias de Indicadores</h5>

            <div class="d-flex align-items-center">
                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                    <i class="bi bi-bar-chart"></i>
                </div>
                <div class="ps-3">
                    <h6><?php echo count($visao['indicadores']); ?></h6>
                <!--  <span class="text-success small pt-1 fw-bold">12%</span> <span class="text-muted small pt-2 ps-1">increase</span> -->

                </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($visao['indicadores'] as $indicador): ?>
                    <tr>
                        <th scope="row"><?php echo $indicador->id; ?></th>
                        <td><?php echo $indicador->name; ?></td>
                        <td><?php echo $indicador->type; ?></td>
                        <td>
                            <!-- <button type="button" class="btn btn-info"><i class="bi bi-pencil-square"></i></button> -->
                            <a href="?p=categorias&id=<?php echo $indicador->id; ?>&type=<?php echo $indicador->type; ?>&n=<?php echo $indicador->name; ?>" class="btn btn-info"><i class="bi bi-pencil-square"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        </div>
    </div>

    <div class="col-xxl-6 col-md-12">
        <div class="card info-card revenue-card">

        <div class="card-body">
            <h5 class="card-title">Categorias de Camadas</h5>

            <div class="d-flex align-items-center">
            <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                <i class="bi bi-bar-chart-steps"></i>
            </div>
            <div class="ps-3">
                <h6><?php echo count($visao['camadas']); ?></h6>
                <!-- <span class="text-success small pt-1 fw-bold">8%</span> <span class="text-muted small pt-2 ps-1">increase</span> -->

            </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($visao['camadas'] as $camada): ?>
                    <tr>
                        <th scope="row"><?php echo $camada->id; ?></th>
                        <td><?php echo $camada->name; ?></td>
                        <td><?php echo $camada->type; ?></td>
                        <td>
                            <a href="?p=categorias&id=<?php echo $camada->id; ?>&type=<?php echo $camada->type; ?>&n=<?php echo $camada->name; ?>" class="btn btn-info"><i class="bi bi-pencil-square"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        </div>
    </div>
</div> 